#!/usr/bin/env python

from optparse import OptionParser
from film_collection import film_collection
from recommendation_system import recommendation_system

parser = OptionParser()
parser.add_option('-r', '--ratings', dest="ratings", default='ratings.csv',
                  help='CSV file with ratings', metavar="FILE_NAME")
parser.add_option('-t', '--titles', dest="titles", default='titles.csv',
                  help='CSV file with titles', metavar="FILE_NAME")
parser.add_option('-m', '--method', help='CSV file with ratings', metavar="METHOD_NAME")
parser.add_option('-x', '--film', help='Film name (association method only)', 
									metavar="FILM_NAME")
parser.add_option('-o', '--output', default='output.csv',
									help='File name of the file for output', metavar="FILE_NAME")

(options, args) = parser.parse_args()


# Read csv files
films = film_collection()

f = open(options.titles)
f.readline()

for line in f.readlines(): films.parse_film(line)
f.close()

f = open(options.ratings)
f.readline()

for line in f.readlines(): films.parse_rating(line)
f.close()

# solve
rs = recommendation_system(films)
top_ten = rs.solve(options.method, options.film)

# output
f = open(options.output, 'w')
print >> f, "film_id|score"

for film in top_ten:
	print >> f, "%i|%g" % (film.id, film.rating)

f.close()

# console
print ""
for film in top_ten:
	print "%i|%g|%s" % (film.id, film.rating, film.title)
