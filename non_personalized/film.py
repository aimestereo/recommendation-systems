class film:

	def __init__(self, id, title):
		self.id = id
		self.title = title
		self.rating = 0
		self.ratings = {}		# { user_id : rating }

	def add_rating(self, user_id, rating):
		self.ratings[int(user_id)] = int(rating)

