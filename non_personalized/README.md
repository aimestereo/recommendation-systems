# Non-personalized recommendation system

### Lecture & Task

[Introduction.pdf] (Introduction.pdf)
[ProgrammingAssignment1.pd] (ProgrammingAssignment1.pdf)

### How it works

###### Prepare

	git clone https://gitlab.com/vvkuznetsov/recommendation-systems.git
	cd recommendation-systems/non_personalized

###### Run tests
	
	./lab1.py -m average -o average.csv
	./lab1.py -m netvotes -o netvotes.csv
	./lab1.py -m positive -o positive.csv
	./lab1.py -m association -x 432423 -o association.csv

###### Results

You can see results in console or in csv files.

* * *
There are labs for course the [Recommendation systems] (http://en.wikipedia.org/wiki/Recommender_system/)
by Tural Gurbanov <tgurbanov@yandex.ru>.