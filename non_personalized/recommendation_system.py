from film_collection import film_collection
import sys

class recommendation_system:
	""" 4 algorithms for Non-personalized recommendation system """

	def __init__(self, films):
		self.films = films

	def solve(self, method, film_id):
		if method == 'average':
			return self.average()
		elif method == 'netvotes':
			return self.netvotes()
		elif method == 'positive':
			return self.positive()
		elif method == 'association':
			return self.association(int(film_id))
		else:
			sys.exit("Error. Metod name: %s didn't recognized." % method)

	def average(self):
		print "Avarage algorithm"

		for film in self.films.values():
			ratings = film.ratings.values()
			film.rating = round( float(sum(ratings)) / len(ratings), 2 )

		return self.top_ten()

	def netvotes(self):
		print "Netvotes algorithm"

		up = down = 0

		for film in self.films.values():
			ratings = film.ratings.values()
			film.rating = reduce(lambda aggr, rating: aggr + (-1, 1)[rating > 5], ratings, 0)

		return self.top_ten()

	def positive(self):
		print "Positive algorithm"

		for film in self.films.values():
			ratings = film.ratings.values()
			positive = reduce(lambda aggr, rating: aggr + (0, 1)[rating > 7], ratings, 0)
			film.rating = round( float(positive) / len(ratings), 2 )

		return self.top_ten()

	def association(self, film_id):
		print "Association algorithm"
		films = self.films

		targeted_film = films.get(film_id)
		if targeted_film is None: sys.exit("Film: %i wasn't found" % film_id)

		x = set(targeted_film.ratings.keys())			# ids of users who like film
		
		users = reduce(lambda u, film:						# ids of all users
									u | set(film.ratings.keys()),
									films.values(), set())
		_x = users - x 														# ids of users who didn't see film

		print "x: %i, !x: %i, user: %i" % (len(x), len(_x), len(users))

		for film in films.values():
			if not film.id == film_id:
				y = xy =_xy = 0

				y = set(film.ratings.keys())					# ids of users who like current film (y)
				
				xy = (x & y)									# ids of users who like both films
				_xy = (_x & y)								# ids of users who don't like targeted film but current film

				film.rating = round( ( float(len(xy)) / len(x) ) / 
														 ( float(len(_xy)) / len(_x) ), 2)

		return self.top_ten()


	def top_ten(self):
		sort = sorted(self.films.values(), key=lambda film: film.rating, reverse=True)
		return sort[:10]
