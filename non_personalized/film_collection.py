from film import film
from collections import MutableMapping

class film_collection(MutableMapping):
	def __init__(self):
		#super(film_collection, self).__init__()
		self.store = dict()

	def parse_film(self, line):
		film_id, title = line.strip().split('|')
		self.store[int(film_id)] = film(int(film_id), title)

	def parse_rating(self, line):
		user_id, film_id, rating = line.strip().split('|')
		if rating.isdigit() and int(rating) > 0:
			self.store[int(film_id)].add_rating(user_id, rating)


# just standart implementation

	def __getitem__(self, key):
	  return self.store[key]

	def __setitem__(self, key, value):
	  self.store[key] = value

	def __delitem__(self, key):
	  del self.store[key]

	def __iter__(self):
	  return iter(self.store)

	def __len__(self):
	  return len(self.store)
